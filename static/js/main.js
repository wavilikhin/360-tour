window.onload = function() {
  // Change paths to images and poses.json
  const pathToJSON = "../panorams/sample-flat/poses.json"
  const pathToImages = '../panorams/sample-flat/images/'
  //
  let container=document.querySelector("#container");let ImagesArr,CoordsArr,RotationArr;function getPanolensCoord(currID,targetID,xyzArr,rotArr){rot_mat=math.matrix(rotArr[currID]);center_curr=math.matrix(xyzArr[currID]);center_tar=math.matrix(xyzArr[targetID]);t=math.multiply(math.multiply(rot_mat,-1),center_curr);tar_cam_mvg=math.add(math.multiply(rot_mat,center_tar),t);tar_cam_mvg=math.multiply(tar_cam_mvg,10);return new THREE.Vector3(tar_cam_mvg.get([2]),-tar_cam_mvg.get([1]),tar_cam_mvg.get([0]))}
  function getInfospotsCoords(firstPano,CoordsArr,RotationArr){infoCoords=[];for(i=0;i<CoordsArr.length;i++){infoCoords.push(getPanolensCoord(firstPano,i,CoordsArr,RotationArr))}
  return infoCoords}
  function getKNNIndexes(infoCoords,knn=7){var coordsWithIxs=[];for(i=0;i<infoCoords.length;i++){coordsWithIxs.push([infoCoords[i],i])}
  coordsWithIxs.sort(function(a,b){return a[0].length()-b[0].length()});var knnIndexes=[];for(i=0;i<knn;i++){knnIndexes.push(coordsWithIxs[i][1])}
  return knnIndexes}
  function toggleInfospots(infospots,isVisible,duration=0){infospots.forEach(element=>{if(isVisible){element.show(duration)}else{element.hide(duration)}})}
  function getPanoramaWithInfospots(panoramaID,ImagesArr,CoordsArr,RotationArr,prevID=NaN){let panoName=ImagesArr[panoramaID];let infoCoords=getInfospotsCoords(panoramaID,CoordsArr,RotationArr);let panorama=new PANOLENS.ImagePanorama(pathToImages+panoName);let infoSpots=[];for(let i=0;i<infoCoords.length;i++){infoSpots.push(new PANOLENS.Infospot(0.3,PANOLENS.DataImage.Info,!0))}
  for(let i=0;i<infoSpots.length;i++){infoSpots[i].position.copy(infoCoords[i])}
  console.log(viewer);for(let i=0;i<infoSpots.length;i++){infoSpots[i].addEventListener("click",function(){viewer.tweenControlCenter(infoSpots[i].position,500);var result=getPanoramaWithInfospots(i,ImagesArr,CoordsArr,RotationArr,panoramaID);var panoramaNext=result[0];var lookAtDir=result[1];toggleInfospots(infoSpots,!1,0);toggleInfospots(panoramaNext.children,!1,0);setTimeout(function(){viewer.add(panoramaNext);viewer.setPanorama(panoramaNext);panoramaNext.addEventListener("load",function(e){panorama.fadeOut(100);viewer.tweenControlCenter(lookAtDir,0);panoramaNext.fadeIn(100)})},500)})}
  var knnIndexes=getKNNIndexes(infoCoords,10);for(let i=0;i<infoSpots.length;i++){if(i!=panoramaID&&knnIndexes.includes(i)){panorama.add(infoSpots[i])}}
  console.log(panorama);var lookAtDir=new THREE.Vector3(1,0,0);if(!Number.isNaN(prevID)){lookAtDir=infoCoords[prevID].negate()}
  return[panorama,lookAtDir]}
  fetch(pathToJSON).then(response=>response.json()).then(result=>{this.console.log(result);ImagesArr=result.images;CoordsArr=result.xyz;RotationArr=result.rotation;viewer=new PANOLENS.Viewer({container:container,autoHideInfospot:!1});let panorama=getPanoramaWithInfospots(33,ImagesArr,CoordsArr,RotationArr)[0];viewer.add(panorama);viewer.setPanorama(panorama);viewer.addUpdateCallback(function(){})})
};